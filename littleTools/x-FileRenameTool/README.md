FileRenameTool  文件重命名工具

#### 项目简介：
FileRenameTool是使用javafx开发的一款文件重命名工具，可快速实现文件批量重命名，支持多种自定义规则，可以快速提高文件重命名的效率节省许多时间。

包括文件的前缀、主文件名、位数、后缀等，可以方便地修改文件的名字，不仅可以重命名文件，还支持替换和删除字符、插入序号，以及转换文件名的大小写等，功能非常丰富，可以满足各种重命名需要，软件的最大特色可以批量重命名，可以对大量的文件同时修改，更加快速高效，解放你的双手，而且可以提前预览改名后的效果，确定没问题再进行保存，不用担心改错的问题


**xJavaFxTool交流QQ群：== [387473650(此群已满)](https://jq.qq.com/?_wv=1027&k=59UDEAD) 请加群②[1104780992](https://jq.qq.com/?_wv=1027&k=bhAdkju9) ==**

#### 环境搭建说明：
- 开发环境为jdk1.8，基于maven构建
- 使用eclipase或Intellij Idea开发(推荐使用[Intellij Idea](https://www.jetbrains.com/?from=xJavaFxTool))
- 该项目为javaFx开发的实用小工具集[xJavaFxTool](https://gitee.com/xwintop/xJavaFxTool)的插件。
- 本项目使用了[lombok](https://projectlombok.org/),在查看本项目时如果您没有下载lombok 插件，请先安装,不然找不到get/set等方法
- 依赖的[xcore包](https://gitee.com/xwintop/xcore)已上传至git托管的maven平台，git托管maven可参考教程(若无法下载请拉取项目自行编译)。[教程地址：点击进入](http://blog.csdn.net/u011747754/article/details/78574026)

![文件重命名工具.png](images/文件重命名工具.png)
![文件重命名工具.gif](images/文件重命名工具.gif)
